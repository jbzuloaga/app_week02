import DOMHandler from '../DOMhandler.js';
import GithubProfile from './GithubProfile.js';


function init() {
  DOMHandler.load(GithubProfile);
}

init()