import DOMHandler from '../DOMhandler.js';
import {getRepos, getUser, getUsers} from './fetch/ApiFetch.js';


let user = null;
let users = null;
let repos = null;
let query = '';
let page = 1;
let error = false;

const GithubProfile = (function() {
  
  const renderUser = (user) => {
    return `
      <div class='user_card'>
        <div class='user_card_image'>
          <img src="${user.avatar_url}" alt="image profile">
        </div>
        <div class='user_info_container'>
          ${ user.name ? `<h2>${user.name}</h2>` : '<h2>No name</h2>'}
        </div>
        <div class='user_info_container'>
          <p>Number of stars:</p>
          <p class='user_info'>${user.followers}</p>
        </div>
        <div class='user_info_container'>
          <p>User repos count:</p>
          <p class='user_info'>${user.public_repos}</p>
        </div>
        <div class='user_info_container'>
          <p>User followers:</p>
          <p class='user_info'>${user.followers}</p>
        </div>
        <div class='user_info_container'>
          <p>User following:</p>
          <p class='user_info'>${user.following}</p>
        </div>
        <div class='user_info_container'>
          <p>User company:</p>
          ${ user.company ? `<p class='user_info'>${user.company}</p>` : '<p>No company</p>'}
        </div>
        <div class='user_info_container'>
        <p>User website:</p>
          ${ user.blog ? `<p class='user_info'>${user.blog}</p>` : '<p>No blog to show</p>'}
        </div>
        <div class='user_info_container'>
        <p>User location:</p>
          ${ user.location ? `<p class='user_info'>${user.location}</p>` : '<p>No location to show</p>'}
        </div>
        <div class='user_info_container'>
        <p>Member since:</p>
          <p class='user_info'>${new Date(user.created_at).toLocaleDateString("en-US")}</p>
        </div>
        <div>
          <a href="${user.html_url}">Link to user</a>
        </div>
      </div>
    `
  }

  const renderRepo = (repo) => {
    return `
      <li class='repo_card'>
        <div>
          <p>Repo name</p>
          <p>${repo.name}</p>
        </div>
        <div>
          <p>Star count</p>
          <p>${repo.stargazers_count}</p>
        </div>
        <div>
          <p>Fork count</p>
          <p>${repo.forks_count}</p>
        </div>
        <div>
        <p>Watchers count</p>
          <p>${repo.watchers_count}</p>
        </div>
        <div>
          <a href="${repo.hmtl_url}">Link to repository</a>
        </div>
      </li>
    `
  }

  const renderList = (user) => {
    return `
      <li class='users_card'>
        <div class='users_image'>
          <img src="${user.avatar_url}" alt="image profile">
        </div>
        <div class='users_username' style='cursor: pointer;'>
          <p>${user.login}</p>
        </div>
      </li>
    `
  }

  const generateTemplate = function() { 
    return `
      <section>
        <form class='input_form'>
          <h1>Github profile search</h1>
          <div class='input_container'>
            <label for='account'>Name</label>
            <input id='account' value=${query}>
          </div>
          <div class='button_container'>
            <button class='submitButton'>Search</button> 
          </div>
        </form>
        ${error ? "<div class='error_message'><h2>No users to show</h2></div>" : ''}
        ${!user ? `
            <div class='users'>
              <ul class='users_list_container'>
                ${users ? (users.items.map((user) => renderList(user))).join('') : ''}
              </ul>
            </div>
          ${users && users.items.length > 0 ? `<div>
            <div class='pagination_container'>
              <div class='pagination previus ${page===1 ? 'pagination_disable':''}'>Previus</div>
              <div>
                <p>${page}/${Math.ceil(users.total_count/10)}</p>
              </div>
              <div class='pagination next ${page===Math.ceil(users.total_count/10) ? 'pagination_disable':''} '>Next</div>
            </div>
          ` : ''}
          </div>` 
          : `
            <div class=user>
              <div class='user_list_container'>
                ${renderUser(user)}
              </div>
            </div>
            <div class='repos_list_container'>
                ${repos.length > 0 ? '<h2>Repository list</h2>' : ''}
                <ul class='repos_list'>
                  ${repos.map((repo) => renderRepo(repo)).join('')}
                </ul>
                ${user ? `
                  <div class='pagination_container'>
                    <div class='pagination previus ${page===1 ? 'pagination_disable':''}'>Previus</div>
                    <div>
                      <p>${page}/${Math.ceil(user.public_repos/10)}</p>
                    </div>
                    <div class='pagination next ${page===Math.ceil(user.public_repos/10) ? 'pagination_disable':''}'>Next</div>
                  </div>
                ` : ''}
            </div>
          `
        }
      </section>
    `
  }

  function HandlerPagination() {
    const pagination = document.querySelectorAll('.pagination');
    pagination.forEach((element) => {
      element.addEventListener('click', async (e) => {
        const previusButton = document.querySelector('.previus');
        const nextButton = document.querySelector('.next');
        switch (e.target.innerText) {
          case 'Previus':
            if(page===1){
              return
            }else{
              page -= 1;
              users ? users = await getUsers(query, page) : repos = await getRepos(query, page)
              DOMHandler.reload();
            }
            break;
          case 'Next':
            const maxPage = users ? Math.ceil(users.total_count/10) : Math.ceil(user.public_repos/10);
            if(page===maxPage){
              return
            }else{
              page += 1;
              users ? users = await getUsers(query, page) : repos = await getRepos(query, page)
              DOMHandler.reload();
            }
            break;
        }
      })
    })
  }

  function selectUser() {
    const selectUser = document.querySelectorAll('.users')
    selectUser.forEach((element) => {
      element.addEventListener('click', async (e) => {
        user = await getUser(e.target.innerText);
        repos = await getRepos(e.target.innerText);
        users = null;
        page = 1;
        DOMHandler.reload();
      });
    })
  }

  // function debounce(func, timeout = 300){
  //   let timer;
  //   return (...args) => {
  //     clearTimeout(timer);
  //     timer = setTimeout(() => { func.apply(this, args); }, timeout);
  //   };
  // }

  // function SearchUser(){
  //   const inputChange = document.getElementById('account');
  //   const handleInput = debounce(async (query) => {
  //     users = await getUsers(query);
  //     console.log(users);
  //     DOMHandler.reload();
  //   }, 2500);
  //   inputChange.addEventListener('input', (e) => {
  //     user = null;
  //     page = 1;
  //     query = e.target.value;
  //   })
  // }
  
  function listenSubmit() {
    const inputForm = document.querySelector('.input_form');
    inputForm.addEventListener('submit', async (e) => {
      e.preventDefault();
      user = null;
      page = 1;
      query = e.target[0].value;
      try {
        users = await getUsers(query);
        error = users.items.length == 0 ? true : false
      } catch (error) {
        console.log(error);
      }
      DOMHandler.reload();
    })
  }


  return {
    toString() {
      return generateTemplate()
    },
    addListeners() {
      listenSubmit(),
      selectUser(),
      HandlerPagination()
      // SearchUser()
    }
  }
})()

export default GithubProfile;