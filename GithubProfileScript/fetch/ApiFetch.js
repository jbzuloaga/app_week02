import {API_TOKEN} from '../config.js';

async function getUsers(query, page = 1) {
  const response = await fetch(`https://api.github.com/search/users?q=${query}&per_page=10&page=${page}`, {
    headers: {
      'Authorization': `Bearer ${API_TOKEN}`,
  },
  });

  let data;
  if (!response.ok) {
    try {
      data = await response.json();
    } catch (error) {
      throw new Error(response.statusText);
    }
    throw new Error(data.errors);
  }

  try {
    data = await response.json();
  } catch (error) {
    data = response.statusText;
  }

  return data;
}

async function getUser(user){

  const response = await fetch(`https://api.github.com/users/${user}`, {
    'header': {
      Authorization: `Token token=${API_TOKEN}`,
      "Content-Type": "application/json",
    }
  });

  let data;
  if (!response.ok) {
    try {
      data = await response.json();
    } catch (error) {
      throw new Error(response.statusText);
    }
    throw new Error(data.errors);
  }

  try {
    data = await response.json();
  } catch (error) {
    data = response.statusText;
  }

  return data;
}

async function getRepos(user, page = 1){

  const response = await fetch(`https://api.github.com/users/${user}/repos?per_page=10&page=${page}`, {
    'header': {
      Authorization: `Token token=${API_TOKEN}`,
      "Content-Type": "application/json",
    }
  });

  let data;
  if (!response.ok) {
    try {
      data = await response.json();
    } catch (error) {
      throw new Error(response.statusText);
    }
    throw new Error(data.errors);
  }

  try {
    data = await response.json();
  } catch (error) {
    data = response.statusText;
  }

  return data;
}

export {getRepos, getUser, getUsers}