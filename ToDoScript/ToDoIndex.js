import Storage from "./Classes/Storage.js";
import TaskList from "./Classes/TaskList.js";
import UI from "./Classes/UI.js";

const storage = new Storage();
const tasks = new TaskList(storage);
const ui = new UI();
const filters = {
  byname: '',
  status: '',
  date: ''
}
tasks.renderList(ui)

//checkboxs logic
const checkboxes = document.querySelectorAll('.cbox');
checkboxes.forEach((element) => {
  element.addEventListener('change', (e) => {
    if (e.target.id === 'cbox1' && e.target.checked) {
      checkboxes[1].checked = false;
      return
    }
    if (e.target.id === 'cbox2' && e.target.checked) {
      checkboxes[0].checked = false;
    }
    if (e.target.id === 'cbox3' && e.target.checked) {
      checkboxes[3].checked = false;
    }
    if (e.target.id === 'cbox4' && e.target.checked) {
      checkboxes[2].checked = false;
    }
  })
})

//Submit logic
const inputSubmit = document.querySelector('.new-task-form');
const buttonSubmit = document.querySelector('#submitButtonForm');
inputSubmit.addEventListener('submit',(e) => {
  e.preventDefault();
  if (e.target[0].value.length < 100 && e.target[0].value !== '') {
    buttonSubmit.setAttribute('disabled', 'disabled');
    setTimeout(() => {
      const name= e.target[0].value;
      const assignee= e.target[1].value;
      const status= e.target[2].checked ? e.target[2].value : e.target[3].value;
      tasks.createTask(name, assignee, status, storage, ui);
      filters.byname = ''
      filters.date = ''
      filters.status = ''
      buttonSubmit.removeAttribute('disabled');
    }, 1000);
  } else {
    console.log('error');
    alert("Name must contain a maximum of 100 characters and can't be empty");
  }
})

//Search by name logic
const searchText = document.querySelector('.searchByTask');
searchText.addEventListener('submit', (e) => {
  e.preventDefault();
  console.log(e.target[0].value);
  tasks.searchName(e.target[0].value, filters, ui);
})

//Search by status logic
const searchStatus = document.querySelectorAll('.filterCbox');
searchStatus.forEach((element) => {
  element.addEventListener('change', (e) => {
    tasks.searchStatus(e.target.checked, e.target.value, filters, ui)
  })
})

//Sort by create date
const sortDate = document.querySelector('.sortByDate');
sortDate.addEventListener('change', (e) => {
  filters.date = e.target.value;
  tasks.sortTasks(storage, e.target.value, ui);
})