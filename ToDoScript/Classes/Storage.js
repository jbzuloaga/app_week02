class Storage {
  getList() {
    if(localStorage.getItem('taskList')) {
      return JSON.parse(localStorage.getItem('taskList'));
    }else {
      return [];
    }
  }

  saveList(list) {
    localStorage.setItem('taskList', JSON.stringify(list));
  }

  updateList(list) {
    localStorage.setItem('taskList', JSON.stringify(list));
  }
}

export default Storage;