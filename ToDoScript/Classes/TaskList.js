import Task from './Task.js';

class TaskList {
  constructor(storage) {
    this.list = storage.getList();
    this.filterList = this.list;
  }

  getNewId() {
    let id = 1;
    if (this.list.length !== 0) {
      id = +this.list[this.list.length - 1].id + 1;
    } else {
      id = 1;
    }
    return id;
  }

  renderList(ui){
    this.filterList.forEach((task) => ui.addTask(task, this));
  }
  
  createTask(name, assignee, status, storage, ui){
    const id = this.getNewId();
    const task = new Task(id, name, assignee, status);
    this.list.push(task);
    this.filterList = this.list;
    ui.addTask(task, this);
    ui.updateID(this);
    storage.saveList(this.list);
  }

  deleteTask(storage, id, ui){
    const newList = this.list.filter((task) => task.id !== id);
    this.list = newList;
    this.filterList = newList;
    ui.deleteTask(id);
    ui.updateID(this);
    storage.saveList(this.list);
  }

  editTask(storage, id, editButton, ui) {
    const task = this.list.find((task) => task.id === id);
    const Filteredtask = this.list.find((task) => task.id === id);
    const {name, assignee, status} = ui.editTask(task, editButton);
    task.name = name;
    task.status = status;
    task.assignee = assignee;
    Filteredtask.name = name;
    Filteredtask.status = status;
    Filteredtask.assignee = assignee;
    console.log(this.filterList);
    storage.saveList(this.list);
  }

  searchName(name, filters, ui) {
    if (name.length === 0) {
      this.filterList = filters.status !== '' ? this.list.filter((task) => task.status === filters.status) : this.list;
      filters.byname = '';
    } else {
      filters.byname = name;
      console.log(filters.status);
      this.filterList = filters.status !== '' ? this.list.filter((task) => task.name.includes(name) && task.status === filters.status) : this.list.filter((task) => task.name.includes(name));
    }
    ui.resetTasks();
    this.renderList(ui);
  }

  sortTasks(storage, type, ui) {
    switch (type) {
      case 'ascendant':
        const tasks = this.filterList.sort((a, b) => new Date(a.date) - new Date(b.date));
        storage.saveList(this.filterList);
        break;
      case 'descendant':
        const task = this.filterList.sort((a, b) => new Date(b.date) - new Date(a.date));
        storage.saveList(this.filterList);
        break;
    }
    ui.resetTasks();
    this.renderList(ui);
  }

  searchStatus(checked, status, filters, ui) {
    if (checked) {
      this.filterList = this.list.filter((task) => task.status === status)
      console.log(this.list);
      filters.status = status;
    } else {
      filters.status = ''
      this.filterList = this.list;
    }
    ui.resetTasks();
    this.renderList(ui);
  }
}

export default TaskList;