import Storage from "./Storage.js";

class UI {
  addTask(task, tasks) {
    const storage = new Storage();
    const listContainer = document.querySelector('.task-list');
    const taskId = document.querySelector('#taskId');
    const taskContainer = document.createElement('li');
    const taskName = document.querySelector('#nameTask');
    taskName.value = '';
    taskId.innerHTML = tasks.list.length + 1;
    taskContainer.setAttribute('id', `task-${task.id}`);
    taskContainer.innerHTML = `
      <div class='task_row1'>
        <div class='content'>
          <h6>ID</h6>
          <p>${task.id}</p>
        </div>
        <div class='content'>
          <h6>Task</h6>
          <input class='task_name' id="input-${task.id}" style='border: none;' type='text' value='${task.name}' readonly/>
        </div>
        <div class='content'>
          <h6>Assignee</h6>
          <select class='task-status select-disabled' id='assignee-${task.id}' disabled>
            <option value="Frank" ${task.assignee==='Frank' ? 'selected':''}>Frank</option>
            <option value="John" ${task.assignee==='John' ? 'selected':''}>John</option>
            <option value="Alice" ${task.assignee==='Alice' ? 'selected':''}>Alice</option>
            <option value="Mary" ${task.assignee==='Mary' ? 'selected':''}>Mary</option>
          </select>
        </div>
        <div class='content'>
          <h6>Status</h6>
          <select class='task-status select-disabled' id='status-${task.id}' disabled>
            <option value="pending" ${task.status==='pending' ? 'selected':''}>Pending</option>
            <option value="done" ${task.status==='done' ? 'selected':''}>Done</option>
          </select>
        </div>
      </div>
      <div class='task_row2'>
        <div class='content'>
          <h6>Creation Date</h6>
          <p>${task.date}</p>
        </div>
        <div class='task_actions'>
          <button class='edit' id='edit-${task.id}' data-id=${task.id}>Edit</button>
          <button class='delete' id='delete-${task.id}' data-id=${task.id}>Delete</button>
        </div>
      </div>`;
    listContainer.appendChild(taskContainer);
    const editButton = document.getElementById(`edit-${task.id}`);
    const deleteButton = document.getElementById(`delete-${task.id}`);
    deleteButton.addEventListener('click', (e) => {
      tasks.deleteTask(storage, task.id, this);
    });
    editButton.addEventListener('click', (e) => {
      tasks.editTask(storage, task.id, editButton, this);
    })
  }
  
  deleteTask(id) {
    document.getElementById(`task-${id}`).remove();
  }
  
  editTask(task, editButton) {
    const inputValue = document.getElementById(`input-${task.id}`)
    const statusValue = document.getElementById(`status-${task.id}`)
    const assigneeValue = document.getElementById(`assignee-${task.id}`)
    if (editButton.innerText.toLowerCase() === 'edit') {
      editButton.innerText = 'Save';
      inputValue.removeAttribute('readonly');
      statusValue.removeAttribute('disabled');
      statusValue.classList.remove('select-disabled');
      assigneeValue.removeAttribute('disabled');
      assigneeValue.classList.remove('select-disabled');
      inputValue.focus();
    } else {
      inputValue.setAttribute("readonly","readonly");
      statusValue.setAttribute('disabled','disabled');
      statusValue.classList.add('select-disabled');
      assigneeValue.setAttribute('disabled','disabled');
      assigneeValue.classList.add('select-disabled');
      editButton.innerText = 'Edit';
      return {name: inputValue.value, assignee: assigneeValue.value, status: statusValue.value}
    }
  }

  updateID(tasks){
    const taskId = document.querySelector('#taskId');
    taskId.innerHTML = tasks.list.length + 1;
  }
  resetTasks(){
    const listContainer = document.querySelector('.task-list');
    listContainer.innerHTML = '';
  }
}

export default UI;